package cripto;

public class CriptoColumnas implements Cripto {
    private Integer cripto;
    
    public CriptoColumnas (Integer cripto) throws Exception{
        if(cripto<0)
            throw new NullPointerException();
        this.cripto = cripto;
    }
    @Override
    public Integer getCripto() {
        return cripto;
    }
    
}
