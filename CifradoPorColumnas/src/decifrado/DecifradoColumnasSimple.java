/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package decifrado;

import cripto.Cripto;
import java.util.LinkedHashMap;
import java.util.Map;
import mensajes.MensajeCifrado;
import mensajes.MensajeSinCifrar;

/**
 *
 * @author luisfer
 */
public class DecifradoColumnasSimple {
    private MensajeCifrado mensajeCifrado;
    private Cripto cripto;
    
    public DecifradoColumnasSimple(MensajeCifrado mensajeCifrado, Cripto cripto)throws Exception{
        if(mensajeCifrado == null || cripto == null)
            throw new Exception();
        this.mensajeCifrado = mensajeCifrado;
        this.cripto = cripto;
    }
    
    private Map<Integer, Character> generarColumna(Integer inicio, Integer fin){
        Map<Integer, Character> columna = new LinkedHashMap();
        Integer llave = 0;
        for(Integer indice = inicio; indice < fin; indice = indice +1){
            columna.put(llave, mensajeCifrado.getMensaje().charAt(indice));
            llave = llave + 1;
        }
        return  columna;
    }
    
    private Map< Integer, Map<Integer,Character> > generarTabla(){
        Integer filas = mensajeCifrado.getMensaje().length()/cripto.getCripto();
        Integer llave = 0;
        Map< Integer, Map<Integer,Character> > tabla = new LinkedHashMap();
        for(Integer indice = 0; indice < mensajeCifrado.getMensaje().length(); indice = indice + filas){
            tabla.put(llave, generarColumna(indice, indice + filas));
            llave = llave + 1;
        }      
        return tabla;
    }
    
    private String mesajePorfila(Integer indiceTabla, Map<Integer,Map<Integer, Character>> tabla){
        String mensaje = "";
        for(Integer indice = 0; indice < tabla.size(); indice = indice + 1 ){
            if(tabla.get(indice).get(indiceTabla) != 'X' && tabla.get(indice).get(indiceTabla) != 'x') 
            mensaje =  mensaje + tabla.get(indice).get(indiceTabla);
        }
        return mensaje;
    }
    
    private String mensajeDecifrado(){
        String mensajeDecifrado = "";
        Map<Integer,Map<Integer, Character>> tabla = generarTabla();
        for(Integer indice = 0; indice < tabla.get(0).size(); indice = indice + 1){
           mensajeDecifrado = mensajeDecifrado + mesajePorfila(indice, tabla);
        }
        return mensajeDecifrado;
    }
    
    public MensajeSinCifrar decifrar(){
        try{
          return new MensajeSinCifrar(mensajeDecifrado());
        }
        catch(Exception e){
            return null;
        }
    }
}
