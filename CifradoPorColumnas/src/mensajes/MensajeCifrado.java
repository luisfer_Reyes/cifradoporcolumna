/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mensajes;

import java.util.LinkedHashMap;
import java.util.Map;


public class MensajeCifrado implements Mensaje{
    private String mensajeCifrado;
    
    public MensajeCifrado (String mensajeCifrado)throws Exception{
        if(mensajeCifrado.isEmpty() || mensajeCifrado == null)
            throw new Exception();
        this.mensajeCifrado = mensajeCifrado;
    }
    
    @Override
    public String getMensaje() {
        return mensajeCifrado;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Map<Integer, Character> aMapa() {
        Map<Integer,Character> mapaMensaje = new LinkedHashMap();
        Integer llave = 0;
        for(Integer indice = 0; indice < mensajeCifrado.length(); indice = indice + 1){
            //if(mensajeCifrado.charAt(indice) != ' '){
                mapaMensaje.put(llave, mensajeCifrado.charAt(indice));
                llave = llave + 1;
            //}
        }
        return mapaMensaje;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
