/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mensajes;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author luisfer
 */
public class MensajeSinCifrar implements Mensaje{
    private String mensajeSinCifrar;

    public MensajeSinCifrar(String mensajeSinCifrar)throws Exception{
        if(mensajeSinCifrar.isEmpty() || mensajeSinCifrar == null)
            throw new Exception("mensaje  no creado");
        this.mensajeSinCifrar = mensajeSinCifrar;
    }    
    @Override
    public String getMensaje() {
        return mensajeSinCifrar;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Map <Integer, Character> aMapa() {
        Map<Integer,Character> mapaMensaje = new LinkedHashMap();
        Integer llave = 0;
        for(Integer indice = 0; indice < mensajeSinCifrar.length(); indice = indice + 1){
           // if(mensajeSinCifrar.charAt(indice) != ' '){
                mapaMensaje.put(llave, mensajeSinCifrar.charAt(indice));
                llave = llave + 1;
           // }
        }
        return mapaMensaje;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
