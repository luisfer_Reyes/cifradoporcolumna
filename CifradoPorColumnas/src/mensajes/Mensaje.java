/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mensajes;

import java.util.Map;

/**
 *
 * @author luisfer
 */
public interface Mensaje {
    
    public String getMensaje();
    public Map<Integer, Character> aMapa();
    
}
