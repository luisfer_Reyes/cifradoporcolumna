package cifradoporcolumnas;

//<<<<<<< HEAD
import cifrado.Cifrado;
import cifrado.CifradorColumnas;
import cripto.Cripto;
import cripto.CriptoColumnas;
import mensajes.Mensaje;
import mensajes.MensajeSinCifrar;

//=======
import cripto.Cripto;
import cripto.CriptoColumnas;
import decifrado.DecifradoColumnasSimple;
import java.util.LinkedHashMap;
import java.util.Map;
import mensajes.Mensaje;
import mensajes.MensajeCifrado;
import mensajes.MensajeSinCifrar;

/**
 *
 * @author luisfer
 */

public class CifradoPorColumnas {

    public static void main(String[] args) {
            
        try{
            
            Mensaje mensaje = new MensajeSinCifrar("Juventud, divino tesoro, ya te vas para no volver");
            System.out.println("mensaje original: "+ mensaje.getMensaje());
            Cripto cripto = new CriptoColumnas (9);
            Cifrado cifrador = new CifradorColumnas(mensaje,cripto);
             MensajeCifrado mensajeCifrado = cifrador.cifrar();
            System.out.println("mensaje cifrado: "+ mensajeCifrado.getMensaje());
            
            DecifradoColumnasSimple decifrado = new DecifradoColumnasSimple(mensajeCifrado, cripto);
            System.out.println("mensaje decifrado: "+decifrado.decifrar().getMensaje());
        }
        catch (Exception e){}

    }
   
}
