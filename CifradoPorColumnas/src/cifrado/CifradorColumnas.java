package cifrado;

import cripto.Cripto;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import mensajes.Mensaje;
import mensajes.MensajeCifrado;
import mensajes.MensajeSinCifrar;

public class CifradorColumnas implements Cifrado{
    private Mensaje mensaje;
    private Cripto cripto;
    
    public CifradorColumnas (Mensaje mensaje, Cripto cripto) throws Exception{
        if(mensaje == null ||cripto == null)
            throw new Exception ("parametros no validos en cifrador");
        this.mensaje = mensaje;
        this.cripto = cripto;
    }
    
   
    private Integer tamanioCaracteres (){
        if (cripto.getCripto() % cripto.getCripto() != 0)
            return mensaje.aMapa().size();
        return mensaje.aMapa().size() + (cripto.getCripto() - (mensaje.aMapa().size() %  cripto.getCripto()));
    }
    
    private Map<Integer, Character> caracterColumna(Integer inicio, Integer fin){
        Map<Integer, Character> columna = new LinkedHashMap();
        Integer llave = 0;
        for(Integer indice = inicio; indice < fin; indice = indice +1){
            if(mensaje.aMapa().get(indice) != null){
                columna.put(llave, mensaje.aMapa().get(indice));
                llave = llave +1;
            }else{
                columna.put(llave, 'x');
                llave = llave +1;
            }
        }
        return columna;
    }
    
    private Map<Integer, Map<Integer, Character>> aTabla(){
        Map<Integer, Map<Integer, Character>> tabla = new LinkedHashMap();
        Integer llave = 0;
        for(Integer indiceTabla = 0; indiceTabla < tamanioCaracteres(); indiceTabla = indiceTabla + cripto.getCripto()){
            tabla.put(llave, caracterColumna(indiceTabla, indiceTabla + cripto.getCripto()));
            llave = llave + 1;
        }
        return tabla;
    }
    
    
    private String mensajeCifrado(){
        String mensajeCifrado = "";
        Map<Integer, Map<Integer, Character>> tabla = aTabla();
        for(Integer indiceColumnas = 0; indiceColumnas < cripto.getCripto(); indiceColumnas = indiceColumnas + 1){
            for(Integer indiceFilas = 0; indiceFilas  < tabla.size(); indiceFilas = indiceFilas +1)
                mensajeCifrado = mensajeCifrado + tabla.get(indiceFilas).get(indiceColumnas);
        }
        return mensajeCifrado;
    }
    
    @Override
    public MensajeCifrado cifrar() {
        try{
            aTabla();
            return new MensajeCifrado(mensajeCifrado());
        }catch(Exception e){return null;}
    }
    
}
