package cifrado;

import mensajes.MensajeCifrado;

public interface Cifrado {
    public MensajeCifrado cifrar();
}
