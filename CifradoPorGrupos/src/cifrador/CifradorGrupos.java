package cifrador;

import cripto.Cripto;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;
import mensaje.Mensaje;
import mensajeCifrado.MensajeCifrado;
import mensajeCifrado.MensajeConCifrado;

public class CifradorGrupos implements Cifrador{
    private Mensaje mensaje;
    private Cripto cripto;
    private Stack grupos;
    public CifradorGrupos (Mensaje mensaje,Cripto cripto)throws Exception{
        if (criptoValido(mensaje,cripto)){
            grupos = new Stack ();
            this.mensaje = mensaje;
            this.cripto = cripto;
            crearGrupos();
        }
        else
            throw new Exception ("El cripto escrito no es valido");
    }
    
    private Boolean criptoValido (Mensaje mensaje,Cripto cripto){       
        return mensaje.mensajeMapa().size() % cripto.getTamanioGrupo() == 0;
    }
    
    private Map <Integer,Character> agregarGrupo(Integer indice,Integer posicionMapa){
        Map <Integer,Character> mapaCifrado =  new HashMap ();
        for (Integer posicion =  indice; posicion < indice + cripto.getTamanioGrupo(); posicion = posicion +1){
            mapaCifrado.put(posicionMapa,mensaje.mensajeMapa().get(posicion));
            posicionMapa = posicionMapa +1;
        }
         return mapaCifrado;
    }
    
    private void crearGrupos(){
        for (Integer indice = 1; indice<= mensaje.mensajeMapa().size(); indice = indice + cripto.getTamanioGrupo())
           grupos.push(agregarGrupo(indice,1));
    }
    
    private Stack cambioCaracter(Map mapa){
        Stack pilaCifrado = new Stack ();
        for (Integer indice = 1;indice <= cripto.getTamanioGrupo() ;indice = indice + 1 ){
            pilaCifrado.push(mapa.get(cripto.getMapaCripto().get(indice)));
        }
        return pilaCifrado;
    }
    
    @Override
    public MensajeCifrado cifrar() {
        try{
            Map <Integer,Stack> mapaPila = new HashMap();
            for (Integer indice = grupos.size(); indice>0;indice = indice -1)
                mapaPila.put(indice, cambioCaracter((Map)grupos.pop()));
            return new MensajeConCifrado(mapaPila) ;
        }
        catch (Exception e){}
        return null;
    }
    
}
