package descifrador;

import cripto.Cripto;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;
import mensaje.Mensaje;
import mensaje.MensajeSinCifrar;
import mensajeCifrado.MensajeCifrado;

public class DescifradorGrupos implements Descifrador{
    private MensajeCifrado  mensaje;
    private Cripto cripto;
    private Stack grupos;
    public DescifradorGrupos(MensajeCifrado mensaje,Cripto cripto) throws Exception{
        if (criptoValido(mensaje,cripto)){
            grupos = new Stack ();
            this.mensaje = mensaje;
            this.cripto = cripto;
        }
        else
            throw new Exception ("El cripto escrito no es valido");
    }
    
    private Boolean criptoValido (MensajeCifrado mensaje,Cripto cripto){       
        return mensaje.getMensaje().length() % cripto.getTamanioGrupo() == 0;
    }
    
    private Map <Integer,Character> agregarGrupo(Integer indice,Integer posicionIndice){
        Map <Integer,Character> mapaCifrado =  new HashMap ();
        for (Integer posicion =  indice; posicion < indice + cripto.getTamanioGrupo(); posicion = posicion +1){
            mapaCifrado.put(cripto.getMapaCripto().get(posicion),(Character)mensaje.getMapa().get(1).pop());
            posicionIndice = posicionIndice + 1;
        }
         return mapaCifrado;
    }
    
    private void crearGrupos(){
        for (Integer indice = 1; indice<= mensaje.getMapa().get(1).size(); indice = indice + cripto.getTamanioGrupo()){
           grupos.push(agregarGrupo(indice,1));}
    }
    
    @Override
    public Mensaje decifrar() {
        try{
           crearGrupos();
           System.out.println(grupos);
            return new MensajeSinCifrar ("h");
        }
        catch (Exception e){}
        return null;
    }
    
}
