package mensajeCifrado;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class MensajeConCifrado implements MensajeCifrado{
    private String mensaje = "";
    private Map <Integer,Stack> mensajeMapa;
    
    public MensajeConCifrado(Map<Integer,Stack> mensajeMapa) throws Exception{
       if (!mensajeMapa.isEmpty()){
           this.mensajeMapa = mensajeMapa;
           mensaje = convertirTexto(mensajeMapa);
       }
       else
           throw new Exception("Error al crear el mensaje");
    }
    
    public MensajeConCifrado(String mensaje) throws Exception{
       if (!mensaje.isEmpty()){
           this.mensaje = mensaje;
           mensajeMapa = convertirMapa(mensaje);        
       }
       else
           throw new Exception("Error al crear el mensaje");
    }
    
    private Map<Integer,Stack > convertirMapa(String mensajeConvertir){
        Stack pila = new Stack();
        for(Integer indice = new Integer (0); indice < mensajeConvertir.length(); indice = indice + 1){
            pila.push(mensajeConvertir.charAt(indice));
        }
        mensajeMapa.put(1,pila);
        return mensajeMapa;
    }
    
    private String convertirTexto(Map<Integer,Stack> mapaAuxiliar){
        for(Integer indice = new Integer (1);indice <= mapaAuxiliar.size();indice = indice +1)
            for (Integer indiceElemento = mapaAuxiliar.get(indice).size()-1;indiceElemento >=0;indiceElemento = indiceElemento -1)
                mensaje = mensaje + mapaAuxiliar.get(indice).elementAt(indiceElemento).toString();
        
        return mensaje;
    }

    @Override
    public String getMensaje() {
        return mensaje;
    }

    @Override
    public Map<Integer, Stack> getMapa() {
        return mensajeMapa;
    }
    
}
