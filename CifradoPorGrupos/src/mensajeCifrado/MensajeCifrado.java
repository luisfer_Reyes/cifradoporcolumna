package mensajeCifrado;

import java.util.Map;
import java.util.Stack;

public interface MensajeCifrado {
    public String getMensaje();
    public Map <Integer,Stack> getMapa();
}
