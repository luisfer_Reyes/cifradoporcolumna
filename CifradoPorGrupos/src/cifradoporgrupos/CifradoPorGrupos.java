package cifradoporgrupos;

import cifrador.Cifrador;
import cifrador.CifradorGrupos;
import cripto.Cripto;
import cripto.Permutacion;
import descifrador.Descifrador;
import descifrador.DescifradorGrupos;
import mensaje.Mensaje;
import mensaje.MensajeSinCifrar;
import mensajeCifrado.MensajeCifrado;
import mensajeCifrado.MensajeConCifrado;

public class CifradoPorGrupos {

    public static void main(String[] args) {
        try{
        Cripto cripto = new Permutacion (81726354);
        Mensaje mensaje = new MensajeSinCifrar ("abcdefgh");
        Cifrador cifrador = new CifradorGrupos (mensaje,cripto);
        MensajeCifrado mensajeCifrado = cifrador.cifrar();
        System.out.println("Texto Original: "+mensaje.getMensaje());
        System.out.println("Cripto: "+cripto.getCripto());
        System.out.println("Cifrado: "+mensajeCifrado.getMensaje());
        
        Descifrador descifrador = new DescifradorGrupos(mensajeCifrado,cripto);
        //descifrador.decifrar();
        }
        catch (Exception exepcion){
            System.err.println(exepcion.getMessage());
        }
    }
    
}
