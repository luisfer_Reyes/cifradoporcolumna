package cripto;

import java.util.HashMap;
import java.util.Map;

public class Permutacion implements Cripto{
    private Integer permutacion;
    private Integer tamanioGrupo;
    
    public Permutacion(Integer permutacion) throws Exception{
        if (criptoValido(permutacion,new Double (Math.log10(permutacion)+1).intValue())&& !numerosRepetidos(permutacion,new Double (Math.log10(permutacion)+1).intValue())){
            this.permutacion = permutacion;
            this.tamanioGrupo = new Double (Math.log10(permutacion)+1).intValue();
        }
        else
            throw new Exception ("No puede haber numeros mayores al tamaño del cripto o numeros repetidos");
    }
    @Override
    public Integer getTamanioGrupo() {
        return tamanioGrupo;
    }

    @Override
    public Integer getCripto() {
        return permutacion;
    }
    
    private Boolean numerosRepetidos(Integer permutacion, Integer tope){
        for (Integer indiceNumero =1 ; indiceNumero < tope; indiceNumero = indiceNumero + 1)
            for (Integer indice =indiceNumero+1 ; indice < tope; indice = indice + 1)
                if(obtenerNumero(permutacion,indiceNumero) == obtenerNumero(permutacion,indice))
                    return new Boolean (true);
        
        return new Boolean (false);
    }
    
    private Boolean criptoValido (Integer permutacion, Integer tamanio){
        for (Integer indice =1 ; indice < tamanio; indice = indice + 1){
            if(obtenerNumero(permutacion,indice) > tamanio)
                return new Boolean (false);
        }
        return new Boolean (true);
    }
    
    private Integer obtenerNumero(Integer numero, Integer divisor){
        Integer digito;
        digito = numero - (numero - ( new Double (numero % Math.pow(10 , divisor) ).intValue() / new Double (Math.pow(10, divisor-1)).intValue()) );
        return digito;
    }
    
    @Override
    public Map<Integer, Integer> getMapaCripto() {
        Map<Integer, Integer> mapaCripto = new HashMap ();
        for (Integer indice = tamanioGrupo; indice >= 1; indice = indice - 1){
            mapaCripto.put(indice,obtenerNumero(permutacion,indice));
        }
        return mapaCripto;
    }
    
}
