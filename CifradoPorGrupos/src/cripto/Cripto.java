package cripto;

import java.util.Map;

public interface Cripto {
    public Integer getTamanioGrupo ();
    public Integer getCripto();
    public Map<Integer,Integer> getMapaCripto ();
}
