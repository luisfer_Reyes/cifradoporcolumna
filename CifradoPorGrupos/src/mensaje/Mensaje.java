package mensaje;

import java.util.Map;

public interface Mensaje {
    public String getMensaje ();
    public Map <Integer,Character> mensajeMapa ();
}
