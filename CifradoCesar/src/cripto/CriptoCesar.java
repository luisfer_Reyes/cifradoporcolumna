package cripto;

public class CriptoCesar implements Cripto {
    private Integer desplazamiento;
    
    public CriptoCesar(Integer desplazamiento) throws Exception{
        if(desplazamiento.intValue()>0)
            this.desplazamiento = desplazamiento;
        else
            throw new NullPointerException();
    }
    
    @Override
    public Integer getCriptograma() {
        return desplazamiento;
    }
    
}
