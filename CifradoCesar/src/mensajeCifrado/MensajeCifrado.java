package mensajeCifrado;

import java.util.Map;

public interface MensajeCifrado {
    public String getMensaje();
    public Map <Integer,Character> getMapa();
}
