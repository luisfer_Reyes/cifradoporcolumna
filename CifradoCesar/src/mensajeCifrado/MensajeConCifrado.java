package mensajeCifrado;

import java.util.Map;

public class MensajeConCifrado implements MensajeCifrado {
    private String mensaje = "";
    private Map <Integer,Character> mensajeMapa;
    
    public MensajeConCifrado(Map<Integer,Character> mensajeMapa) throws Exception{
       if (!mensajeMapa.isEmpty()){
        mensaje = convertirTexto(mensajeMapa);
        this.mensajeMapa = mensajeMapa;
       }
       else
           throw new Exception("Error al crear el mensaje");
    }
    
    private String convertirTexto(Map<Integer,Character> mensajeMapa){
        for(Integer indice = new Integer (1);indice <= mensajeMapa.size();indice = indice +1)
                mensaje = mensaje + mensajeMapa.get(indice).toString(); 
        return mensaje;
    }

    @Override
    public String getMensaje() {
        return mensaje;
    }

    @Override
    public Map<Integer, Character> getMapa() {
        return mensajeMapa;
    }
}
