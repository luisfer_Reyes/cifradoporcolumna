package cifrador;

import mensajeCifrado.MensajeCifrado;

public interface Cifrador {
    public MensajeCifrado cifrador();
}
