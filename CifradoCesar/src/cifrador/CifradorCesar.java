package cifrador;

import alfabeto.Alfabeto;
import cripto.Cripto;
import cripto.CriptoCesar;
import java.util.HashMap;
import java.util.Map;
import mensaje.Mensaje;
import mensaje.MensajeSinCifrar;
import mensajeCifrado.MensajeCifrado;
import mensajeCifrado.MensajeConCifrado;

public class CifradorCesar implements Cifrador{
    private Mensaje mensaje;
    private Cripto cripto;
    private Alfabeto alfabeto;
    
    public CifradorCesar (Mensaje mensaje, Cripto cripto, Alfabeto alfabeto) throws Exception{
        if(!mensaje.getMensaje().isEmpty() && cripto.getCriptograma()>0 && !alfabeto.getAlfabeto().isEmpty()){
        this.mensaje = mensaje;
        this.cripto = cripto;
        this.alfabeto = alfabeto;}
        else
            throw new Exception ("Error al crear el Cifrador");
    }
    
    private Integer posicionCifrado(Character caracter){
        Integer posicion = (alfabeto.getIndiceCaracter(caracter)+cripto.getCriptograma())%alfabeto.getAlfabeto().size();
        
        if(posicion == 0)
            return alfabeto.getAlfabeto().size();
            
        return posicion;
    }
    
    private Character elemento (Integer posicion){
        if( mensaje.mensajeMapa().get(posicion) == ' ')
            return mensaje.mensajeMapa().get(posicion);
        
        return alfabeto.getCaracter(posicionCifrado(mensaje.mensajeMapa().get(posicion)));
    }
    @Override
    public MensajeCifrado cifrador() {
        try{
            Map <Integer,Character> mapaMensaje = new HashMap();
            for(Integer indice = new Integer (1); indice <= mensaje.mensajeMapa().size(); indice = indice + 1){ 
                mapaMensaje.put(indice,elemento(indice));
            }
            return new MensajeConCifrado (mapaMensaje);
        }
        catch (Exception e){}
       return null;      
    }
        
}
