package mensaje;

import java.util.HashMap;
import java.util.Map;

public class MensajeSinCifrar implements Mensaje{
    private String mensaje;
    
    public MensajeSinCifrar (String mensaje) throws Exception{
        if(!mensaje.isEmpty())
            this.mensaje = mensaje;
        else
            throw new Exception ("Error al crear el mensaje");
    }

    @Override
    public String getMensaje() {
        return mensaje;
    }
    @Override
    public Map<Integer, Character> mensajeMapa() {
        Map <Integer,Character> mapaMensaje = new HashMap ();
        for(Integer indice =  new Integer (0);indice < mensaje.length(); indice = indice + 1){
            mapaMensaje.put(indice+1,new Character (mensaje.charAt(indice))); }
        return mapaMensaje;
    }

}
