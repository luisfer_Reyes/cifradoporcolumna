package descifrador;

import mensaje.Mensaje;

public interface Descifrador {
    public Mensaje descifrar(); 
}
