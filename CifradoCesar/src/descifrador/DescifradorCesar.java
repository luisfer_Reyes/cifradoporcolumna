package descifrador;

import alfabeto.Alfabeto;
import cripto.Cripto;
import cripto.CriptoCesar;
import java.util.HashMap;
import java.util.Map;
import mensaje.Mensaje;
import mensaje.MensajeSinCifrar;
import mensajeCifrado.MensajeCifrado;
import mensajeCifrado.MensajeConCifrado;

public class DescifradorCesar implements Descifrador {
    private MensajeCifrado mensaje;
    private Cripto cripto;
    private Alfabeto alfabeto;
    
    public DescifradorCesar(MensajeCifrado mensaje,Cripto cripto,Alfabeto alfabeto) throws Exception{
        if(!mensaje.getMensaje().isEmpty() && cripto.getCriptograma()>0 && !alfabeto.getAlfabeto().isEmpty()){
            this.mensaje = mensaje;
            this.cripto = cripto;
            this.alfabeto = alfabeto;
        }
        else 
            throw new Exception("Error al creal el Descifrador");
    }
    
    
    private Integer posicionCifrado(Character caracter){
        Integer posicion = (alfabeto.getIndiceCaracter(caracter)-cripto.getCriptograma())%alfabeto.getAlfabeto().size();
        
        if(posicion == 0)
            return 27;
            
        return posicion;
    }
            
    private Character elemento (Integer posicion){
        if( mensaje.getMapa().get(posicion) == ' ')
            return mensaje.getMapa().get(posicion);
        
        return alfabeto.getCaracter(posicionCifrado(mensaje.getMapa().get(posicion)));
    }
    
    @Override
    public Mensaje descifrar() {
        try{
            String mensajeDescifrado ="";
        
             for(Integer indice = new Integer (1); indice <= mensaje.getMapa().size(); indice = indice + 1){ 
                mensajeDescifrado = mensajeDescifrado +elemento(indice);
             }
             return new MensajeSinCifrar(mensajeDescifrado);
        }
        catch (Exception e){}
        return null;
    }
    
    
}
