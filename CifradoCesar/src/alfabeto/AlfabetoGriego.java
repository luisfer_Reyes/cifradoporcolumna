package alfabeto;

import java.util.HashMap;
import java.util.Map;

public class AlfabetoGriego implements Alfabeto{
    private Map <Integer,Character> alfabeto;
    
    public AlfabetoGriego (){
       alfabeto =  new HashMap();
       alfabeto.put(1,new Character('a'));;alfabeto.put(2,new Character ('β'));alfabeto.put(3,new Character('γ'));alfabeto.put(4,new Character('δ'));
       alfabeto.put(5,new Character('ε'));alfabeto.put(6,new Character('ζ'));  alfabeto.put(7,new Character ('η'));alfabeto.put(8,new Character('θ'));
       alfabeto.put(9,new Character('ι'));alfabeto.put(10,new Character('κ')); alfabeto.put(11,new Character('λ')); alfabeto.put(12,new Character ('μ'));
       alfabeto.put(13,new Character('ν'));alfabeto.put(14,new Character('ξ')); alfabeto.put(15,new Character('ο'));alfabeto.put(16,new Character('π')); 
       alfabeto.put(17,new Character ('ρ'));alfabeto.put(18,new Character('ς'));alfabeto.put(19,new Character('σ'));alfabeto.put(20,new Character('τ'));
       alfabeto.put(21,new Character('υ'));alfabeto.put(22,new Character('φ')); alfabeto.put(23,new Character ('χ'));alfabeto.put(24,new Character('ψ'));
       alfabeto.put(25,new Character('ω'));
    }

    @Override
    public Map<Integer, Character> getAlfabeto() {
        return alfabeto;
    }

    @Override
    public Boolean caracterEnAlfabeto(Character caracter) {
        return new Boolean( alfabeto.containsValue(caracter));
    }

    @Override
    public Character getCaracter(Integer indice) {
         return alfabeto.get(indice);
    }

    @Override
    public Integer getIndiceCaracter(Character caracter) {
        if(caracterEnAlfabeto(caracter)){
            for(Integer indice = new Integer (1);indice <= alfabeto.size();indice = indice +1){
                if (alfabeto.get(indice).charValue() == caracter)
                    return indice;
            }
        }
        return new Integer (-1);
    }
    
}
