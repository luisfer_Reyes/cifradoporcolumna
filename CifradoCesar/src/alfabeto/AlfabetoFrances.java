package alfabeto;

import java.util.HashMap;
import java.util.Map;

public class AlfabetoFrances implements Alfabeto{
    private Map<Integer, Character> alfabeto;
    
    public AlfabetoFrances(){
       alfabeto = new HashMap();
       alfabeto.put(1, new Character ('a')); alfabeto.put(2, new Character ('b')); alfabeto.put(3, new Character ('c')); alfabeto.put(4, new Character ('d'));
       alfabeto.put(5, new Character ('e')); alfabeto.put(6, new Character ('f')); alfabeto.put(7, new Character ('g')); alfabeto.put(8, new Character ('h'));
       alfabeto.put(9, new Character ('i')); alfabeto.put(10, new Character ('j')); alfabeto.put(11, new Character ('k')); alfabeto.put(12, new Character ('l'));
       alfabeto.put(13, new Character ('m')); alfabeto.put(14, new Character ('n')); alfabeto.put(15, new Character ('o')); alfabeto.put(16, new Character ('p'));
       alfabeto.put(17, new Character ('q')); alfabeto.put(18, new Character ('r')); alfabeto.put(19, new Character ('s')); alfabeto.put(20, new Character ('t'));
       alfabeto.put(21, new Character ('u')); alfabeto.put(22, new Character ('v')); alfabeto.put(23, new Character ('w')); alfabeto.put(24, new Character ('x'));
       alfabeto.put(25, new Character ('y')); alfabeto.put(26, new Character ('z')); alfabeto.put(27, new Character ('á')); alfabeto.put(28, new Character ('â'));
       alfabeto.put(29, new Character ('é')); alfabeto.put(30, new Character ('è')); alfabeto.put(31, new Character ('ë')); alfabeto.put(32, new Character ('ë'));
       alfabeto.put(33, new Character ('î')); alfabeto.put(34, new Character ('ï')); alfabeto.put(35, new Character ('ô')); alfabeto.put(36, new Character ('ù'));
       alfabeto.put(37, new Character ('û')); alfabeto.put(38, new Character ('ü')); alfabeto.put(39, new Character ('ÿ')); alfabeto.put(40, new Character ('Ç'));
       alfabeto.put(41, new Character ('æ')); alfabeto.put(42, new Character ('œ'));
    }
    @Override
    public Map<Integer, Character> getAlfabeto() {
        return alfabeto;
    }

    @Override
    public Boolean caracterEnAlfabeto(Character caracter) {
        return new Boolean( alfabeto.containsValue(caracter) );
    }

    @Override
    public Character getCaracter(Integer indice) {
        return alfabeto.get(indice);
    }

    @Override
    public Integer getIndiceCaracter(Character caracter) {
if(caracterEnAlfabeto(caracter)){
            for(Integer indice = new Integer (1);indice <= alfabeto.size();indice = indice +1){
                if (alfabeto.get(indice).charValue() == caracter)
                    return indice;
            }
        }
        return new Integer (-1);    }
    
}
