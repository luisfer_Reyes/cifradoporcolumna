package alfabeto;

import java.util.Map;

public interface Alfabeto {
    public Map<Integer,Character> getAlfabeto();
    public Boolean caracterEnAlfabeto(Character caracter);
    public Character getCaracter (Integer indice);
    public Integer getIndiceCaracter (Character caracter);
}
