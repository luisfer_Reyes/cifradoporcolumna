package cifradocesar;

import alfabeto.Alfabeto;
import alfabeto.AlfabetoEspanol;
import alfabeto.AlfabetoFrances;
import alfabeto.AlfabetoGriego;
import cifrador.Cifrador;
import cifrador.CifradorCesar;
import cripto.Cripto;
import cripto.CriptoCesar;
import descifrador.Descifrador;
import descifrador.DescifradorCesar;
import mensaje.Mensaje;
import mensaje.MensajeSinCifrar;
import mensajeCifrado.MensajeCifrado;


public class CifradoCesar {
   
    public static void main(String[] args) {
        try{
        System.out.println("-----------Español------------");
        Mensaje mensaje = new MensajeSinCifrar ("hola mundo cruel");
        Alfabeto alfabeto = new AlfabetoEspanol();
        Cripto cripto = new CriptoCesar (new Integer (3));
        Cifrador crifrador = new CifradorCesar(mensaje,cripto,alfabeto);
        MensajeCifrado mensajeCifrado = crifrador.cifrador();
        System.out.println("Mensaje Original: "+mensaje.getMensaje());
        System.out.println("Mensaje Cifrado: "+mensajeCifrado.getMensaje());
        
        System.out.println("-----------Griego------------");
        Mensaje mensajeGriego = new MensajeSinCifrar ("αβγδεζηθικλμνξοπρςστυφχψω");
        Alfabeto alfabetoGriego = new AlfabetoGriego();
        Cripto criptoGriego = new CriptoCesar (new Integer (5));
        Cifrador cifradorGriego = new CifradorCesar(mensajeGriego,criptoGriego,alfabetoGriego);
        
        MensajeCifrado mensajeCifradoGriego = cifradorGriego.cifrador();
        System.out.println("Mensaje Original: "+mensajeGriego.getMensaje());
        System.out.println("Mensaje Cifrado: "+mensajeCifradoGriego.getMensaje());
        
        System.out.println("-----------Frances------------");
        Mensaje mensajeFrances = new MensajeSinCifrar ("hôla mündó crùelÿï");
        Alfabeto alfabetoFrances = new AlfabetoFrances();
        Cripto criptoFrances = new CriptoCesar (new Integer (8));
        Cifrador cifradorFrances = new CifradorCesar(mensajeFrances,criptoFrances,alfabetoFrances);
        
        
        MensajeCifrado mensajeCifradoFrances = cifradorFrances.cifrador();
        System.out.println("Mensaje Original: "+mensajeFrances.getMensaje());
        System.out.println("Mensaje Cifrado: "+mensajeCifradoFrances.getMensaje());
        }
        catch (Exception e){
            System.err.println(e);
        }
    }
    
}
