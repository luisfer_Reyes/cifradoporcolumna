package mensaje;

import alfabetos.Alfabeto;
import java.util.Map;

public interface Mensaje {
    
    public String getMensaje(); 
    public boolean perteneceAlfabeto(Alfabeto alfabeto);
    public Map<Integer,Character> mensajeMapa();
    //public Map<Integer,Object> convertirAmapa();
}
