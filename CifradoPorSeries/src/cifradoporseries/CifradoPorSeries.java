
package cifradoporseries;

import alfabetos.Alfabeto;
import alfabetos.AlfabetoRuso;
import alfabetos.Español;
import cifrado.Cifrador;
import cifrado.CifradorPorSeries;
import cripto.Cripto;
import cripto.CriptoCifrador;
import decifrado.Decifrador;
import decifrado.Decifrar;
import mensaje.Mensaje;
import mensaje.MensajeSinCifrar;
import mensajeCifrado.MensajeCifrado;
import series.Serie;
import series.Series;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import mensajeCifrado.MensajeCifrador;

public class CifradoPorSeries {

    
    public static void main(String[] args) {
        try{
            Alfabeto alfabetoRuso = new AlfabetoRuso();
            Mensaje mensaje = new MensajeSinCifrar("НОГШЛЯЬЖ");
           // Mensaje mensaje = new MensajeSinCifrar("ELAUTENTICOSOÑADORESELQUESUEÑAIMPOSIBLES");
            System.out.println("mensaje: "+mensaje.getMensaje());
            Serie serie = new Series();
            
            Map <Integer,Set<Integer>> series = new LinkedHashMap();
            series.put(1,serie.multiplosDe(mensaje.getMensaje().length(),4));     
            series.put(2,serie.numerosImpares(mensaje.getMensaje().length()));
            series.put(3,serie.numerosPares(mensaje.getMensaje().length()));
        
            Cripto cripto = new CriptoCifrador(series);
            Alfabeto alfabetoEspañol = new Español();
            Cifrador cifrador = new CifradorPorSeries(mensaje,cripto, alfabetoRuso);
        
            MensajeCifrador mensajeCifrado = cifrador.Cifrar();
            System.out.println("mensaje cifrado: "+mensajeCifrado.getMensajeCifrado());
        
            Decifrar decifrador = new Decifrador(mensajeCifrado, cripto);
            System.out.println("mensaje decifrado: "+decifrador.decifrarMensaje().getMensaje());
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
    
}
