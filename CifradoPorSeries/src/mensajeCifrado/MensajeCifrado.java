package mensajeCifrado;

import cripto.Cripto;


public class MensajeCifrado implements MensajeCifrador{
    
    private String mensajeCifrado;
    private Cripto cripto;
    
    public MensajeCifrado (String mensajeCifrado, Cripto cripto)throws Exception{
        if(mensajeCifrado == null || cripto == null)
            throw new Exception("error al crear la clase mensajeCifrado");
        this.mensajeCifrado = mensajeCifrado;
        this.cripto = cripto;
    }

    @Override
    public String getMensajeCifrado() {
        return mensajeCifrado;
    }

    
}
