package series;

import java.util.Set;


public interface Serie {
    public Set <Integer> numerosPares(Integer numeroDeLetras); 
    public Set <Integer> numerosImpares(Integer numeroDeLetras); 
    public Set <Integer> multiplosDe(Integer numeroDeLetras,Integer multiplo); 
}
