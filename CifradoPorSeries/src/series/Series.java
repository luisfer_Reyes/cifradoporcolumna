package series;


import java.util.LinkedHashSet;
import java.util.Set;


public class Series implements Serie {

    private boolean esPrimo(Integer numero){
        if(numero < 2)
            return false;
        for(Integer indice = 2; indice < numero; indice = indice + 1)
            if(numero % indice == 0)
                return false;
        return true;
    }
    
    public Set<Integer> numerosPrimos(Integer numeroDeLetras){
        Set <Integer> setPrimos = new LinkedHashSet<Integer>();
         for(Integer indice = 1; indice <= numeroDeLetras; indice = indice + 1)
            if(esPrimo(indice))
                setPrimos.add(indice);
        
        return setPrimos;
    }
    
    public Set<Integer> numerosPares(Integer numeroDeLetras) {
       Set <Integer> setPares = new LinkedHashSet<Integer>();
         for(Integer indice = 1; indice <= numeroDeLetras; indice = indice + 1)
            if(indice % 2 == 0)
                setPares.add(indice);
        
        return setPares;
    }


    public Set<Integer> numerosImpares(Integer numeroDeLetras) {
        Set <Integer> setImpares = new LinkedHashSet<Integer>();
         for(Integer indice = 1; indice <= numeroDeLetras; indice = indice + 1)
            if(indice % 2 != 0)
                setImpares.add(indice);
        
        return setImpares;
    }
    public Set<Integer> numerosNaturales(Integer numeroDeLetras){
        Set <Integer> setNumerosNaturales = new LinkedHashSet<Integer>();
        for(Integer indice = 1; indice <= numeroDeLetras; indice = indice + 1)
                setNumerosNaturales.add(indice);
        return setNumerosNaturales;
    }

  
    public Set<Integer> multiplosDe(Integer numeroDeLetras,Integer multiplo) {
        Set <Integer> setMultiplo = new LinkedHashSet<Integer>();
         for(Integer indice = 1; indice <= numeroDeLetras; indice = indice + 1)
            if(indice % multiplo == 0)
                setMultiplo.add(indice);
        
        return setMultiplo;
    }
    
}
