/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alfabetos;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author luisfer
 */
public class AlfabetoGriego implements Alfabeto{
    
    Map<Integer,Character> alfabetoGriegoComun = new LinkedHashMap();
    
    public AlfabetoGriego(){
       alfabetoGriegoComun.put(0,'α'); alfabetoGriegoComun.put(1,'β'); alfabetoGriegoComun.put(2,'γ'); 
       alfabetoGriegoComun.put(3,'δ'); alfabetoGriegoComun.put(4,'ε'); alfabetoGriegoComun.put(5,'ζ');
       alfabetoGriegoComun.put(6,'η'); alfabetoGriegoComun.put(7,'θ'); alfabetoGriegoComun.put(8,'ι');
       alfabetoGriegoComun.put(9,'κ'); alfabetoGriegoComun.put(10,'λ'); alfabetoGriegoComun.put(11,'μ');
       alfabetoGriegoComun.put(12,'ν'); alfabetoGriegoComun.put(13,'ξ'); alfabetoGriegoComun.put(14,'ο');
       alfabetoGriegoComun.put(15,'π'); alfabetoGriegoComun.put(16,'ρ'); alfabetoGriegoComun.put(17,'ς');   
       alfabetoGriegoComun.put(18,'τ'); alfabetoGriegoComun.put(19,'υ'); alfabetoGriegoComun.put(20,'φ');
       alfabetoGriegoComun.put(21,'χ'); alfabetoGriegoComun.put(22,'ψ'); alfabetoGriegoComun.put(23,'ω');   
 
    }

    @Override
    public Map<Integer, Character> getAlfabeto() {
        return alfabetoGriegoComun;
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Map<Character, Integer> mapaAlfabeto() {
        Map < Character, Integer> alfabeto = new LinkedHashMap();
        for(Integer indice = new Integer(0); indice < alfabetoGriegoComun.size(); indice ++){
            alfabeto.put( alfabetoGriegoComun.get(indice) ,indice);
        }
        return  alfabeto;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
     
    
    
}
