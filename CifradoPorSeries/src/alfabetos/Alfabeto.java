/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alfabetos;

import java.util.Map;


public interface Alfabeto {
    public Map< Integer,Character> getAlfabeto();
    public Map<  Character,Integer > mapaAlfabeto();
}
