package cripto;


import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

public class CriptoCifrador implements Cripto{
    private Map <Integer,Set<Integer>> mapaSeries;
    
    public CriptoCifrador(Map <Integer,Set<Integer>> mapaSeries) throws Exception{
        if(mapaSeries == null)
            throw new Exception("error en la creacion de la clase CriptoCifrador");
        this.mapaSeries =  mapaSeries;
    }
    
    @Override
    public Set<Integer> getCripto() {
        Set<Integer> cripto = new LinkedHashSet<Integer>();
        mapaSeries.forEach((k,v) -> cripto.addAll(v));   
        return cripto;
    }
    
    
}
