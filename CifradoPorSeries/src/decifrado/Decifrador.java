/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package decifrado;

import cripto.Cripto;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import mensaje.Mensaje;
import mensaje.MensajeSinCifrar;
import mensajeCifrado.MensajeCifrador;

/**
 *
 * @author luisfer
 */
public class Decifrador implements Decifrar{
    private MensajeCifrador mensajeCifrado;
    private Cripto cripto;
    
    public Decifrador(MensajeCifrador mensajeCifrado, Cripto cripto)throws Exception{
        if(mensajeCifrado == null || cripto == null)
            throw new Exception("error al crear la clase Decifrador");
        this.mensajeCifrado = mensajeCifrado;
        this.cripto = cripto;
    }
    
    private void mapaDecifrado(Map <Integer, Character> mapaMensaje){
        Integer contador = new Integer(0);
        for( Iterator it = cripto.getCripto().iterator(); it.hasNext();) { 
           mapaMensaje.put((Integer)it.next(),mensajeCifrado.getMensajeCifrado().charAt(contador));
           contador = contador + 1;
	}
    }
    private String decifrar( Map <Integer, Character> mapaMensaje, String mensajeDecifrado){
        for (Integer indice = new Integer(1); indice <= mensajeCifrado.getMensajeCifrado().length(); indice = indice +1){
            mensajeDecifrado = mensajeDecifrado + ""+mapaMensaje.get(indice);
        } 
        return mensajeDecifrado;
    }
    @Override 
    public Mensaje decifrarMensaje() throws Exception {
        Map <Integer, Character> mapaMensaje = new HashMap();
        String mensajeDecifrado = "";
        mapaDecifrado(mapaMensaje);
        if(mapaMensaje.size() != mensajeCifrado.getMensajeCifrado().length())
            throw new Exception("no es posible decifrar");
        return new MensajeSinCifrar(decifrar(mapaMensaje ,mensajeDecifrado));        
        
    }

        
}
