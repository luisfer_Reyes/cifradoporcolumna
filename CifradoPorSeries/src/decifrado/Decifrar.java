/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package decifrado;

import mensaje.Mensaje;

/**
 *
 * @author luisfer
 */
public interface Decifrar {
    
    public Mensaje decifrarMensaje() throws Exception;

}
