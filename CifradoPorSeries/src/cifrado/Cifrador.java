package cifrado;


import mensajeCifrado.MensajeCifrado;

public interface Cifrador {

    public  MensajeCifrado Cifrar() throws Exception;    
}
