package cifrado;

import alfabetos.Alfabeto;
import cripto.Cripto;
import mensajeCifrado.MensajeCifrado;
import java.util.Iterator;
import mensaje.Mensaje;

public class CifradorPorSeries implements Cifrador{
    private Mensaje mensajeSinCifrar;
    private Cripto cripto;
    private Alfabeto alfabeto;
    
    public CifradorPorSeries (Mensaje mensajeSinCifrar,Cripto cripto, Alfabeto alfabeto)throws Exception{
        if(mensajeSinCifrar == null || cripto == null || alfabeto == null)
            throw new Exception("error en la clase CifradoPorSeries");
        this.mensajeSinCifrar = mensajeSinCifrar;
        this.cripto=cripto;
        this.alfabeto = alfabeto;
    }
    private String hacerCifrado(String mensajeCifrado){
        for( Iterator it = cripto.getCripto().iterator(); it.hasNext();) { 
            mensajeCifrado = mensajeCifrado + mensajeSinCifrar.getMensaje().charAt((Integer)it.next()-1);
	}
        return mensajeCifrado;
    }
    
    @Override
    public MensajeCifrado Cifrar() throws Exception {
        try{
            String mensajeCifrado="";
            if(cripto.getCripto().size() != mensajeSinCifrar.getMensaje().length()|| !mensajeSinCifrar.perteneceAlfabeto(alfabeto))
                throw new Exception("no se puede cifrar ");
            return new MensajeCifrado(hacerCifrado(mensajeCifrado), cripto);
        }
        catch(Exception e){System.err.println(e.getMessage());}
        return null;
    }
   
}
