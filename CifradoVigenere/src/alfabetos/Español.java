/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alfabetos;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author luisfer
 */
public class Español implements Alfabeto{
    
    Map <Integer, Character> alfabetoEspañol = new LinkedHashMap();
    
    public Español(){
       alfabetoEspañol.put(0,'A'); alfabetoEspañol.put(1,'B'); alfabetoEspañol.put(2,'C'); alfabetoEspañol.put(3,'D'); alfabetoEspañol.put(4,'E');
       alfabetoEspañol.put(5,'F'); alfabetoEspañol.put(6, 'G'); alfabetoEspañol.put(7,'H'); alfabetoEspañol.put(8,'I'); alfabetoEspañol.put(9,'J');
       alfabetoEspañol.put(10,'K'); alfabetoEspañol.put(11,'L'); alfabetoEspañol.put(12,'M'); alfabetoEspañol.put(13,'N'); alfabetoEspañol.put(14,'O');
       alfabetoEspañol.put(15,'P'); alfabetoEspañol.put(16, 'Q'); alfabetoEspañol.put(17,'R'); alfabetoEspañol.put(18,'S'); alfabetoEspañol.put(19,'T');
       alfabetoEspañol.put(20,'U'); alfabetoEspañol.put(21,'V'); alfabetoEspañol.put(22,'W'); alfabetoEspañol.put(23,'X'); alfabetoEspañol.put(24,'Y'); 
       alfabetoEspañol.put(25,'Z'); alfabetoEspañol.put(26,'Ñ');
    }
    
    @Override
    public Map< Integer,Character> getAlfabeto() {
        return alfabetoEspañol;
    }

    @Override
    public Map< Character, Integer> mapaAlfabeto() {
        Map < Character, Integer> alfabeto = new LinkedHashMap();
        for(Integer indice = new Integer(0); indice < alfabetoEspañol.size(); indice ++){
            alfabeto.put( alfabetoEspañol.get(indice) ,indice);
        }
        return  alfabeto;
    }
    
}
