/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alfabetos;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author luisfer
 */
public class AlfabetoRuso implements Alfabeto{
    
    Map <Integer, Character> alfabetoRuso = new LinkedHashMap();
    
    public AlfabetoRuso(){
       alfabetoRuso.put(0,'А'); alfabetoRuso.put(1,'Б'); alfabetoRuso.put(2,'В'); alfabetoRuso.put(3,'Г'); alfabetoRuso.put(4,'Д');
       alfabetoRuso.put(5,'Е'); alfabetoRuso.put(6,'Ё'); alfabetoRuso.put(7,'Ж'); alfabetoRuso.put(8,'З'); alfabetoRuso.put(9,'И');
       alfabetoRuso.put(10,'Й'); alfabetoRuso.put(11,'К'); alfabetoRuso.put(12,'Л'); alfabetoRuso.put(13,'М'); alfabetoRuso.put(14,'Н');
       alfabetoRuso.put(15,'О'); alfabetoRuso.put(16,'П'); alfabetoRuso.put(17,'Р'); alfabetoRuso.put(18,'С'); alfabetoRuso.put(19,'Т');
       alfabetoRuso.put(20,'У'); alfabetoRuso.put(21,'Ф'); alfabetoRuso.put(22,'Х'); alfabetoRuso.put(23,'Ц'); alfabetoRuso.put(24,'Ч'); 
       alfabetoRuso.put(25,'Ш'); alfabetoRuso.put(26,'Щ'); alfabetoRuso.put(27,'Ъ'); alfabetoRuso.put(28,'Ы'); alfabetoRuso.put(29,'Ь'); 
       alfabetoRuso.put(30,'Э'); alfabetoRuso.put(31,'Ю'); alfabetoRuso.put(32,'Я');
    }
    
    @Override
    public Map<Integer, Character> getAlfabeto() {
        return alfabetoRuso;
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Map<Character, Integer> mapaAlfabeto() {
        Map < Character, Integer> alfabeto = new LinkedHashMap();
        for(Integer indice = new Integer(0); indice < alfabetoRuso.size(); indice ++){
            alfabeto.put( alfabetoRuso.get(indice) ,indice);
        }
        return  alfabeto;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
