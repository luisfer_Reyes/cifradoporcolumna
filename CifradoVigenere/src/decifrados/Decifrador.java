/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package decifrados;

import alfabetos.Alfabeto;
import claves.Clave;
import criptogramas.Cripto;
import criptogramas.Criptograma;
import mensajes.Mensaje;
import mensajes.MensajeSinCifrar;

/**
 *
 * @author luisfer
 */
public class Decifrador implements Decifrar{
    private Alfabeto alfabeto;
    private Criptograma criptograma;
    private Clave clave;

    public Decifrador(Alfabeto alfabeto, Criptograma criptograma, Clave clave)throws Exception{
        if(alfabeto == null || criptograma  == null || clave == null)
            throw new Exception("error el el decifrador");
        this.alfabeto = alfabeto;
        this.criptograma = criptograma;
        this.clave= clave;
    }
    
    private Integer calcularPosicion(Integer posicionCriptograma, Integer posicionClave){
        Integer numero = posicionCriptograma - posicionClave;
        if(numero <= 0)
            numero = numero + alfabeto.getAlfabeto().size();
        return numero % alfabeto.getAlfabeto().size();
    }
    private String decifrarMensaje(){
        String cifrado="";
        for(Integer indice = new Integer(0); indice < criptograma.getCriptograma().length(); indice = indice +1){
            Integer posicionCriptograma = new Integer(alfabeto.mapaAlfabeto().get(criptograma.getCriptograma().charAt(indice)));
            Integer posicionClave = new Integer(alfabeto.mapaAlfabeto().get(clave.completarClave(criptograma.getCriptograma().length()).get(indice)));
            Integer posicion = new Integer(calcularPosicion(posicionCriptograma,posicionClave));
            cifrado = cifrado + alfabeto.getAlfabeto().get(posicion);
        }
        return cifrado;
    }
    @Override
    public Mensaje decifrar() throws Exception{
        if(!criptograma.perteneceAlfabeto(alfabeto) || !clave.perteneceAlfabeto(alfabeto))
            throw new Exception("no es posible cifrar");
        return new MensajeSinCifrar(decifrarMensaje());
    }
    
    
}
