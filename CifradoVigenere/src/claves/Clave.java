/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package claves;

import alfabetos.Alfabeto;
import java.util.Map;
import mensajes.Mensaje;

/**
 *
 * @author luisfer
 */
public interface Clave {
    
    public String getClave();
    public boolean perteneceAlfabeto(Alfabeto alfabeto);
    public Map<Integer, Character> completarClave(Integer tamañoMensaje);
    
}
