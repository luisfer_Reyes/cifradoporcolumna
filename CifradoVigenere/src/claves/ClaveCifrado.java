/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package claves;

import alfabetos.Alfabeto;
import java.util.LinkedHashMap;
import java.util.Map;
import mensajes.Mensaje;


public class ClaveCifrado implements Clave{
    private String clave;
    
    public ClaveCifrado(String clave)throws Exception{
        if(clave.isEmpty() || clave == null)
            throw new Exception("Error en la clave");
        this.clave = clave;
    }
    @Override
    public String getClave() {
        return clave;
    }    
    private boolean siEsta(Character caracter, Alfabeto alfabeto){
        if(alfabeto.mapaAlfabeto().get(caracter.charValue()) != null)
                return true;
        return false;
    }
    @Override
    public boolean perteneceAlfabeto(Alfabeto alfabeto) {
        for(Integer indice = 0; indice < clave.length(); indice = indice + 1){
            if(clave.charAt(indice)!= ' ')
                if(!siEsta(clave.charAt(indice), alfabeto))
                    return false;
        }
        return true;
    }
    private Map<Integer, Character> sinEspacio(){
        Map<Integer, Character> mapaClave = new LinkedHashMap();
        Integer contador = new Integer(0);
        for(Integer indice = new Integer(0); indice < clave.length(); indice = indice + 1){
            if(clave.charAt(indice)!= ' '){
                mapaClave.put(contador, clave.charAt(indice));
                contador = contador +1;
            }
        }
        return mapaClave;
    }
    @Override
    public Map<Integer, Character> completarClave(Integer tamañoMensaje) {
        Map<Integer, Character> mapaClave = new LinkedHashMap();
        Integer contador = new Integer(0);
        for(Integer indice = new Integer(0); indice < tamañoMensaje; indice = indice + 1){
            if(contador == sinEspacio().size())
                contador = 0;
            mapaClave.put(indice, sinEspacio().get(contador));
            contador = contador +1;
        }
        return mapaClave;
    }
    
    
}
