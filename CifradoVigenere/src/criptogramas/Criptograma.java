/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package criptogramas;

import alfabetos.Alfabeto;


public interface Criptograma {
    public String getCriptograma();
    public boolean perteneceAlfabeto(Alfabeto alfabeto);
}
