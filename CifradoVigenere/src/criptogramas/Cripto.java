/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package criptogramas;

import alfabetos.Alfabeto;

/**
 *
 * @author luisfer
 */
public class Cripto implements Criptograma {
    private String criptograma;
    
    public Cripto (String criptograma)throws Exception{
        if(criptograma.isEmpty()|| criptograma == null)
            throw new Exception("Error en el criptograma");
        this.criptograma = criptograma;
    }
    
    @Override
    public String getCriptograma() {
        return criptograma;
    }

    private boolean siEsta(Character caracter, Alfabeto alfabeto){
        if(alfabeto.mapaAlfabeto().get(caracter.charValue()) != null)
                return true;
        return false;
    }
    
    @Override
    public boolean perteneceAlfabeto(Alfabeto alfabeto) {
        for(Integer indice = 0; indice < criptograma.length(); indice = indice + 1){
            if(!siEsta(criptograma.charAt(indice), alfabeto))
                return false;
        }
        return true;    
    }
    
}
