/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mensajes;

import alfabetos.Alfabeto;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author luisfer
 */
public class MensajeSinCifrar implements Mensaje{
    private String mensaje;
    
    public MensajeSinCifrar(String mensaje)throws Exception{
        if(mensaje.isEmpty()||mensaje == null)
            throw new Exception("error en el mensaje");
        this.mensaje = mensaje;
    }
    
    
    @Override
    public String getMensaje() {
        return mensaje;
    }
    private boolean siEsta(Character caracter, Alfabeto alfabeto){
        if(alfabeto.mapaAlfabeto().get(caracter.charValue()) != null)
                return true;
        return false;
    }
    @Override
    public boolean perteneceAlfabeto(Alfabeto alfabeto) {
        for(Integer indice = 0; indice < mensaje.length(); indice = indice + 1){
            if(mensaje.charAt(indice) != ' ')
                if(!siEsta(mensaje.charAt(indice), alfabeto))
                    return false;
        }
        return true;
    }

    @Override
    public Map<Integer, Character> mensajeMapa() {
        Map <Integer, Character> mapaMensaje=new LinkedHashMap();
        Integer llave = new Integer(0);
        for(Integer indice = new Integer(0); indice < mensaje.length(); indice = indice +1){
            if( mensaje.charAt(indice) != ' '){
                mapaMensaje.put(llave, mensaje.charAt(indice));
                llave = llave +1;
            }
        }
        return mapaMensaje;
    }
    
}
