/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cifrados;

import alfabetos.Alfabeto;
import claves.Clave;
import criptogramas.Cripto;
import criptogramas.Criptograma;
import mensajes.Mensaje;

/**
 *
 * @author luisfer
 */
public class Cifrado implements Cifrador{
    private Alfabeto alfabeto;
    private Mensaje mensaje;
    private Clave clave;
    
    public Cifrado (Alfabeto alfabeto, Mensaje mensaje, Clave clave)throws Exception{
        if(alfabeto == null || mensaje == null || clave == null )
            throw new Exception("error en el cifrado");
        this.alfabeto = alfabeto;
        this.mensaje = mensaje;
        this.clave = clave;
    }
    
    private String cifrarMensaje(){
        String cifrado="";
        for(Integer indice = new Integer(0); indice < mensaje.mensajeMapa().size(); indice = indice +1){
            //cifrado = cifrado + alfabeto.getAlfabeto().get((mensaje.mensajeMapa().get(indice)+clave.completarClave(mensaje.mensajeMapa().size()).get(indice))% alfabeto.getAlfabeto().size());
            Integer posicionMensaje = new Integer(alfabeto.mapaAlfabeto().get(mensaje.mensajeMapa().get(indice)));
            Integer posicionClave = new Integer(alfabeto.mapaAlfabeto().get(clave.completarClave(mensaje.mensajeMapa().size()).get(indice)));
            cifrado = cifrado + alfabeto.getAlfabeto().get((posicionMensaje + posicionClave) % alfabeto.getAlfabeto().size() );
            //System.out.println((mensaje.getMensaje().charAt(indice)));
        }
        return cifrado;
    }
    
    @Override
    public Criptograma cifrar() throws Exception{
        
        if(!mensaje.perteneceAlfabeto(alfabeto) || !clave.perteneceAlfabeto(alfabeto))
            throw new Exception("no es posible cifrar");
        return new Cripto(cifrarMensaje());
    }
    
}
