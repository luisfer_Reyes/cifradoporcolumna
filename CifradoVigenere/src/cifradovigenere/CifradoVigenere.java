
package cifradovigenere;

import alfabetos.Alfabeto;
import alfabetos.AlfabetoGriego;
import alfabetos.AlfabetoRuso;
import alfabetos.Español;
import cifrados.Cifrado;
import cifrados.Cifrador;
import claves.Clave;
import claves.ClaveCifrado;
import criptogramas.Cripto;
import criptogramas.Criptograma;
import decifrados.Decifrador;
import decifrados.Decifrar;
import mensajes.Mensaje;
import mensajes.MensajeSinCifrar;

/**
 *
 * @author luisfer
 */
public class CifradoVigenere {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try{
            //Alfabeto alfabetoEspañol = new Español();
            //Alfabeto alfabetoGriego = new AlfabetoGriego();
            Alfabeto alfabetoRuso = new AlfabetoRuso();
            Mensaje mensaje = new MensajeSinCifrar("НОГШЛЯЬЖ");
            System.out.println("mensaje: "+mensaje.getMensaje());
          
            Clave clave = new ClaveCifrado("РФМ");
            
            Cifrador cifrado = new Cifrado(alfabetoRuso,mensaje,clave);
            
            Criptograma criptograma = cifrado.cifrar();
            
            System.out.println("cifrado: "+cifrado.cifrar().getCriptograma());
            
            Decifrar decifrador = new Decifrador(alfabetoRuso, criptograma, clave);
            System.out.println("decifrado: "+decifrador.decifrar().getMensaje());
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
}
