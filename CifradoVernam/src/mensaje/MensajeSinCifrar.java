/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mensaje;

import binario.Binario;
import binario.LetraBinario;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 *
 * @author luisfer
 */
public class MensajeSinCifrar implements Mensaje{

    private String mensaje;
    
    public MensajeSinCifrar(String mensaje)throws Exception{
        if(mensaje.isEmpty() || mensaje == null)
            throw new Exception("error en el mensaje");
        this.mensaje = mensaje;
    }
    
    @Override
    public String getMensaje() {
        return mensaje;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    private String caracteraBinario(Character caracter){
        Integer numero = new Integer(caracter.toString().codePointAt(0));
        String binario = "";
        while(numero!=0){
                Integer digito = new Integer (numero % 2);
                binario= digito+binario;
                numero = numero/2;
        }
        return binario;
    }
    
    @Override
    public Set<Binario> aBinario() {
        Set<Binario> palabraBinario = new LinkedHashSet<Binario>();
        for(Integer indice = new Integer(0); indice < mensaje.length(); indice = indice +1){
            palabraBinario.add( new LetraBinario(caracteraBinario(mensaje.charAt(indice))));
        }
        return palabraBinario;
    }
    
    
}
