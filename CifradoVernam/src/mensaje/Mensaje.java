/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mensaje;

import binario.Binario;
import java.util.Set;

/**
 *
 * @author luisfer
 */
public interface Mensaje {
    
    public String getMensaje();
     public Set<Binario> aBinario(); 
}
