/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aleatorios;

import binario.Binario;
import binario.LetraBinario;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author luisfer
 */
public class BinariosAleatorios implements Aleatorio{
    
    public BinariosAleatorios(){}
    
    private Binario aleatorios(){
            String binario="";
        for(Integer indice = new Integer(0); indice < 7; indice = indice + 1 ){
            Integer aleatorio = new Integer((int)(Math.random()*2));
            binario = aleatorio + binario; 
        }
        return new LetraBinario(binario);
    }
    @Override
    public Map<Integer, Binario> getAleatorio(Integer tamano) {
        Map<Integer, Binario> binariosAleatorios = new LinkedHashMap<Integer, Binario>();
        for(Integer indice = new Integer(0); indice < tamano; indice = indice + 1 ){
            binariosAleatorios.put(indice,aleatorios());
        }
        return binariosAleatorios;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
