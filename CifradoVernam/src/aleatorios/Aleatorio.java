/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aleatorios;

import binario.Binario;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author luisfer
 */
public interface Aleatorio {
    
    public Map<Integer, Binario> getAleatorio(Integer tamaño);
    
}
