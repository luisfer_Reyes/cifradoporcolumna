
package cifrado;

import aleatorios.Aleatorio;
import aleatorios.BinariosAleatorios;
import binario.Binario;
import binario.LetraBinario;
import clave.ClaveBinario;
import criptos.Cripto;
import criptos.Criptografia;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import mensaje.Mensaje;
import mensaje.MensajeSinCifrar;



public class CifradoVernam implements Cifrado{
    private Mensaje mensajeSinCifrar;
    private ClaveBinario claveMensaje;
    private Map<Integer,Binario> binariosAleatorios ;
    private Cripto criptograma;
    
    public CifradoVernam(Mensaje mensajeSinCifrar, ClaveBinario claveMensaje,Map<Integer,Binario> binariosAleatorios )throws Exception{
        if(mensajeSinCifrar == null || claveMensaje == null || binariosAleatorios == null)
            throw new Exception("error en el cifrado");
        this.mensajeSinCifrar = mensajeSinCifrar;
        this.claveMensaje = claveMensaje;
        this.binariosAleatorios =  binariosAleatorios;
        this.criptograma = new Criptografia(cifrar());
    }

    private String xor(Binario binarioClave, Binario binarioAleatorio){
        String xor="";
        for(Integer indice = new Integer(0); indice < binarioClave.getNumeroBinario().length(); indice = indice +1){
            if(binarioClave.getNumeroBinario().charAt(indice)==binarioAleatorio.getNumeroBinario().charAt(indice))
                xor = xor + '1';
            else
                xor = xor + '0';
        } 
        return xor;
    }
    
    private Set<Binario> hacerCifrado(){
        Set<Binario> cripto = new LinkedHashSet<Binario>();
        Integer contador = new Integer(0);
        for( Iterator it = claveMensaje.getClaveBinario().iterator(); it.hasNext();) {
            Binario binarioClave = (Binario)it.next();
            cripto.add( new LetraBinario(xor( binarioClave,binariosAleatorios.get(contador))));
        }
        return cripto;
    }
    
    @Override
    public Set<Binario> cifrar() throws Exception {
        if(binariosAleatorios.size() != claveMensaje.getClaveBinario().size())
            throw new Exception("no coinciden los bianarios");
        return hacerCifrado();
    //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private Set<Binario> hacerDecifrado(){
        Set<Binario> decifrado = new LinkedHashSet<Binario>();
        Integer contador = new Integer(0);
        for( Iterator it = criptograma.getCriptografia().iterator(); it.hasNext();) {
            Binario binarioCripto = (Binario)it.next();
            decifrado.add( new LetraBinario(xor( binarioCripto,binariosAleatorios.get(contador))));
        }
        return decifrado;
    }
    
    private Integer binarioAentero(String binario){
        Integer numero = new Integer(0); 
        Integer exponente = new Integer(0);
        for(Integer indice = new Integer(binario.length()-1); indice >= 0; indice = indice -1){
            numero  = numero +  (Integer.parseInt(""+binario.charAt(indice)))*(int)Math.pow(2,exponente);
            exponente = exponente +1;
        }
        return numero;
    }
    
    private Set<Integer> decifradoLista(){
        Set<Binario> decifrado = hacerDecifrado();
        Set<Integer> numeros = new LinkedHashSet<Integer>();
        Integer sumatoria = new Integer(0);
        for( Iterator it = decifrado.iterator(); it.hasNext();) {
            Binario binarioCripto = (Binario)it.next();
            numeros.add(binarioAentero(binarioCripto.getNumeroBinario()));
        }
        return numeros;
    }
    
    private String decifradoConcatenar(){
        Set<Integer> numeros = decifradoLista();
        String mensaje="" ;
        for( Iterator it = numeros.iterator(); it.hasNext();) {
            Integer binarioCripto = (Integer)it.next();
            Character caracter = new Character((char)binarioCripto.intValue());
            mensaje = mensaje + caracter;
        }
        return mensaje;
    }
    
    @Override
    public Mensaje decifrar() throws Exception{
        if(binariosAleatorios.size() != claveMensaje.getClaveBinario().size())
            throw new Exception("no coinciden los bianarios");
        return new MensajeSinCifrar(decifradoConcatenar());
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
