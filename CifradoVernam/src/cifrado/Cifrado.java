/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cifrado;

import binario.Binario;
import java.util.Set;
import mensaje.Mensaje;

/**
 *
 * @author luisfer
 */
public interface Cifrado {
    
    public Set<Binario> cifrar() throws Exception;
    public Mensaje decifrar()throws Exception;
}
