/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package binario;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 *
 * @author luisfer
 */
public class LetraBinario implements Binario{

     private String numeroBinario;
    
    public LetraBinario(String numeroBinario){
        this.numeroBinario = numeroBinario;
    }
    @Override
    public String getNumeroBinario() {
       return numeroBinario;
    } 
    
    
}
