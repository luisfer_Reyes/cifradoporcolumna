/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Alfabetos;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 *
 * @author luisfer
 */
public class AlfabetoEspañol implements Alfabeto{
    private Set<Character> alfabeto = new LinkedHashSet<Character>();
    
    public AlfabetoEspañol(){
        alfabeto.add('A');alfabeto.add('B');alfabeto.add('C');alfabeto.add('D');alfabeto.add('E'); 
        alfabeto.add('F');alfabeto.add('G');alfabeto.add('H');alfabeto.add('I');alfabeto.add('J');
        alfabeto.add('K');alfabeto.add('L');alfabeto.add('M');alfabeto.add('N');alfabeto.add('0');
        alfabeto.add('P');alfabeto.add('Q');alfabeto.add('R');alfabeto.add('S');alfabeto.add('T');
        alfabeto.add('U');alfabeto.add('V');alfabeto.add('W');alfabeto.add('X');alfabeto.add('Y'); alfabeto.add('Z');
    
    }
    
    @Override
    public Set<Character> getAlfabeto() {
        return alfabeto;
    }
    
}
