/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Alfabetos;

import java.util.Set;

/**
 *
 * @author luisfer
 */
public interface Alfabeto {
    
    public Set<Character> getAlfabeto();
    
}
