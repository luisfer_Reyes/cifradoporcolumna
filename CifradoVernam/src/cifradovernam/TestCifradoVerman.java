/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cifradovernam;

import Alfabetos.Alfabeto;
import Alfabetos.AlfabetoEspañol;
import aleatorios.Aleatorio;
import aleatorios.BinariosAleatorios;
import binario.Binario;
import cifrado.Cifrado;
import cifrado.CifradoVernam;
import clave.ClaveBinario;
import clave.ClaveMensaje;
import java.util.Iterator;
import mensaje.Mensaje;
import mensaje.MensajeSinCifrar;

/**
 *
 * @author luisfer
 */
public class TestCifradoVerman {
 
    public static void main (String args[]){
        try{
            Alfabeto alfabetoEspañol = new AlfabetoEspañol();
        
            Mensaje mensaje = new MensajeSinCifrar("VERMAN");
            ClaveBinario clave = new ClaveMensaje(mensaje.aBinario());
            Aleatorio binariosA = new BinariosAleatorios();
            Cifrado cifrado = new CifradoVernam(mensaje, clave, binariosA.getAleatorio(mensaje.getMensaje().length()));
            for( Iterator it = cifrado.cifrar().iterator(); it.hasNext();) {
                Binario binario = (Binario)it.next();
                System.out.println(binario.getNumeroBinario());
            }
            System.out.println(cifrado.decifrar().getMensaje());
        }catch(Exception e){
        }
    }
}
